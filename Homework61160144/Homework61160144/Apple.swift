//
//  Apple.swift
//  Homework61160144
//
//  Created by student on 12/18/20.
//

import SwiftUI

struct Apple: View {
    var body: some View {
        HStack(alignment: .top){
        Image("logo")
            .resizable()
            .frame(width: 80.0, height: 80.0)
            VStack(alignment: .leading){
                HStack(spacing: 4.0){
                  Text("Apple")
                  Image(systemName: "checkmark.seal.fill")
                  .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/)
                  .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                  Text("@Apple")
                 .font(.footnote)
                 .foregroundColor(Color.secondary)
                }
                Text("นำ iPhone ของคุณมาแลก แล้วอัพเกรดเป็น iPhone 12 mini ใหม่")
                    .font(.footnote)
                Image("post")
                    .resizable()
                    .frame(width: 200.0, height: 250.0)
                Text("ขอแนะนำ iPhone 12 mini")
                    .font(.footnote)
                Text("apple.com")
                    .font(.footnote)
                    .foregroundColor(Color.secondary)
            
                    .padding(.bottom, 5.0)
                HStack{
                    Image(systemName: "message")
                        .foregroundColor(Color.gray)
                        .padding(10.0)
                        .frame(width: 10.0, height: 5.0)
                    Text("1").font(.footnote)
                        .foregroundColor(Color.gray)
                    Image(systemName: "arrow.2.squarepath")
                        .foregroundColor(Color.gray)
                        .padding(.leading, 20.0)
                    Text("11").font(.footnote)
                        .foregroundColor(Color.gray)
                    Image(systemName: "heart")
                        .foregroundColor(Color.gray)
                        .padding(.leading, 15.0)
                    Text("28").font(.footnote)
                        .foregroundColor(Color.gray)
                    Image(systemName: "square.and.arrow.up")
                        .foregroundColor(Color.gray)
                        .padding(.leading, 15.0)
                }
                HStack{
                Image(systemName: "arrow.up.forward.app.fill")
                    .foregroundColor(Color.gray)
                Text("Promoted")
                    .font(.footnote)
                    .foregroundColor(Color.gray)
                    .multilineTextAlignment(.leading)
                }
                
                
            }
                
        }
    }
}

struct Apple_Previews: PreviewProvider {
    static var previews: some View {
        Apple()
    }
}
