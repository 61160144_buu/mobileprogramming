//
//  Homework61160144App.swift
//  Homework61160144
//
//  Created by student on 12/18/20.
//

import SwiftUI

@main
struct Homework61160144App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
