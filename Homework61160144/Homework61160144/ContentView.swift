//
//  ContentView.swift
//  Homework61160144
//
//  Created by student on 12/18/20.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HStack{
            Image("mango")
                .resizable()
                .frame(width: 120.0, height: 150.0)
                .aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fit/*@END_MENU_TOKEN@*/)
            VStack(alignment: .leading){
                Text("Mango Smoothie")
                    .fontWeight(.bold)
                Text("Mango, Banana, Almon Milk")
                    .font(.footnote)
                Text("140 Calories")
                    .font(.footnote)
                    .foregroundColor(Color.secondary)
            HStack{
                Image(systemName:"star.fill")
                    .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/)
                    .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
            Image(systemName:"star.fill")
                .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/)
                .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
            Image(systemName:"star.fill")
                .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/)
                .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
            Image(systemName:"star.fill")
                .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/)
                .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
            Image(systemName:"star.fill")
                .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/)
                .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
            }
            }
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
        ContentView()
            .preferredColorScheme(.dark)
    }
}
